# "Great" Software Developers Quotes

Quotes and behavior from some of the "greatest" software developers and managers you have ever worked with.

1. Refactoring? That's a job for the next developers.

2. Refactoring? Why spending time on this shit job? You should go learn something useful like [React](https://medium.com/airbnb-engineering/sunsetting-react-native-1868ba28e30a) [Native](https://engineering.udacity.com/react-native-a-retrospective-from-the-mobile-engineering-team-at-udacity-89975d6a8102)

3. I can fire all backend engineers and the system can still run.

4. Asking a senior software developer to generate ERD diagram and refuse to press a few buttons on the SQL clients the team is using.

5. Write a 10 pages email to demean her direct report to her boss.

6. You don't even listen to what senior said. You are junior, so you should not question my authority.

7. It should be easy.

8. It should be 2 lines of code.

9. After a drastic change of requirements, asked, "Why it is so hard to change?"

10. The old way works, why need refactoring?

11. Just stick to the old way, because it works.

12. Why not putting `AirplanePricing` and `OrangePricing` in the same domain of `Pricing`?